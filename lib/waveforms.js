Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

var socket = io();

var wavesurfer = WaveSurfer.create({
    container: '#waveform',
    scrollParent: true
});

var selectingDivide = false;

var playBtn = document.getElementById("play");
var next = document.getElementById("next");
var divide = document.getElementById("divide");

socket.on('wordFile', function(data) {
    wavesurfer.load(data);
});

wavesurfer.on('ready', function () {
    playBtn.disabled = false;
    divide.disabled = false;
});

next.onclick = function() {
    socket.emit('nextFile');
    playBtn.disabled = false;
    divide.disabled = false;
}

divide.onclick = function() {
    selectingDivide = true;
    divide.disabled = true;

    const container = document.getElementById("container");
    const buttons = container.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].classList.add('green');
    }
}

playBtn.onclick = function() {
    wavesurfer.play();
}

var removeFn = function(btnId, peakValue) {
    return function() {
        var loading = document.getElementById('loading');
        loading.classList.add('loading-true');

        if (selectingDivide) {
            const buttons = document.getElementsByClassName('fa-times-circle-o');
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].classList.remove('green');
            }
            socket.emit('setDivider', {peakValue});
            selectingDivide = false;
        } else {
            document.getElementById(btnId).remove();
            socket.emit('removePeak', {peakValue});
        }
    }
}

function drawFloatArray(samples, canvas, peaks) {
    var i, n = samples.length;
    var dur = (n / 48000 * 16000)>>0;
    canvas.title = 'Duration: ' +  dur / 1000.0 + 's';

    var width = canvas.width;
    var height = canvas.height;
    var ctx = canvas.getContext('2d');
    ctx.strokeStyle = 'yellow';
    ctx.fillStyle = '#303030';
    ctx.fillRect(0,0,width,height);
    ctx.beginPath();
    ctx.moveTo(0,height/2);


    for (i = 0; i < n; i++) {
        x = ((i*width) / n);
        y = (-(samples[i]*height/2)+height/2);
        ctx.lineTo(x, y);

        if (peaks.includes(i)) {
            btn = document.createElement("BUTTON");
            var id = 'button_' + i;
            btn.id = id;
            btn.classList.add('to-be-removed');
            btn.onclick = removeFn(id, i);
            btn.classList.add('btn'); btn.classList.add('fa'); btn.classList.add('fa-times-circle-o'); btn.classList.add('fa-lg');
            btn.style.cssText = "position: absolute; left: " + (x - 15) + "px; top: " + (y - 10	) + "px;";
            document.getElementById("container").appendChild(btn);
        }
    }

    ctx.stroke();
    ctx.closePath();
    canvas.mBuffer = samples;
}

socket.on('end', function() {
    document.getElementsByClassName('to-be-removed').remove()
    var currentCanvas = document.getElementById('canvas_id');
    if (currentCanvas) {
        currentCanvas.remove();
    }

    var node = document.getElementById("waveform");
    node.innerHTML = '<strong> Thats all, thank you! </strong>';
});

socket.on('unlock', function() {
    var loading = document.getElementById('loading');
    loading.classList.remove('loading-true');
});

socket.on('data', function(payload) {
    var data = payload.data;
    var peaks = payload.peaks;

    document.getElementsByClassName('to-be-removed').remove()
    var currentCanvas = document.getElementById('canvas_id');
    if (currentCanvas) {
        currentCanvas.remove();
    }

    var array = []; var i = 0;
    while (data.channelData[0][i] !== undefined) {
        array.push(data.channelData[0][i]);
        i++;
    }

    var canvas = document.createElement("canvas");
    canvas.width  = window.innerWidth;
    canvas.height = 800;
    canvas.id = 'canvas_id'

    drawFloatArray(array, canvas, peaks);

    var node = document.getElementById("container");
    node.appendChild(canvas);
})
