
const express = require('express');
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const wav = require('node-wav');

sqlite3.verbose()

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');

    io.on('connection', (socket) => {
        let db = new sqlite3.Database('./db/synthesis.db', (err) => {
            if (err) { return console.error(err.message); }
            console.log('Connected to the SQlite database.');
        });


        let sql = `SELECT id, word FROM words;`;
        let words = [];
        db.all(sql, [], (err, rows) => {
            if (rows) { words = rows; }
        });;

        let i = 0;
        let sendWord = () => {
            if (i < words.length) {
                socket.emit('word', words[i].word);
                i++;
            } else {
                socket.emit('end', true);
            }
        }

        socket.on('nextWord', (result) => {
            if (result.file) {
                const filePath = `words/${words[i - 1].word}.mp3`;

                fs.writeFile(filePath, result.file, "utf8", (err) => {
                    sql = `INSERT INTO samples(word_id, file_path) VALUES(?, ?);`
                    db.run(sql, [words[i - 1].id, filePath], (err) => {
                        if (err) { console.log(err.message) } else { console.log(`File saved.`) }
                    });
                    console.log(`The file ${words[i - 1].word}.mp3 was succesfully saved!`);
                    sendWord();
                });
            } else {
                sendWord();
            }
        });

        socket.on('disconnect', () => {
            console.log('A user disconnected');
            if (db) {
                db.close((err) => {
                    if (err) {
                      return console.error(err.message);
                    }
                    console.log('Closed the database connection.');
                  });
            }
        });

    });
});
let db;

app.get('/waveforms', (req, res) => {
    res.sendFile(__dirname + '/waveforms.html');

    io.on('connection', (socket) => {
        if (!db) {
            db = new sqlite3.Database('./db/synthesis.db', (err) => {
                if (err) { return console.error(err.message); }
                console.log('Connected to the SQlite database.');
            });
        }

        socket.on('disconnect', () => {
            console.log('A user disconnected');
            if (db) {
                db.close((err) => {
                    if (err) {
                      return console.error(err.message);
                    }
                    console.log('Closed the database connection.');
                  });
                db = undefined;
            }
        });

        let sql = `SELECT words.id, word, peaks, file_path FROM words JOIN peaks ON words.id = peaks.word_id JOIN samples ON words.id = samples.word_id;`;
        let words = [];
        db.all(sql, [], (err, rows) => {
            if (rows) { words = rows; }
        });

        const transformPeaks = (peaks) => {
            return peaks.split(' ').map(peak => parseInt(peak));
        }

        let i = 0;
        socket.on('nextFile', () => {
            if (i < words.length) {
                const wav_path = words[i].file_path.replace('mp3', 'wav')

                let buffer = fs.readFileSync(wav_path);
                let result = wav.decode(buffer);

                socket.emit('wordFile', words[i].file_path.split('/')[1]);
                socket.emit('data', {
                    data: result,
                    peaks: transformPeaks(words[i].peaks),
                });

                i++;
            } else {
                socket.emit('end', true);
            }
        });

        socket.on('removePeak', (data) => {
            const currentI = i - 1;

            if (currentI >= 0) {
                let peaks = transformPeaks(words[currentI].peaks);
                const index = peaks.indexOf(data.peakValue);

                if (index !== -1) {
                    peaks.splice(index, 1);
                }
                const peaksString = peaks.join(' ');
                const sql = `UPDATE peaks SET peaks=? WHERE word_id=?;`;
                db.run(sql, [peaksString, words[currentI].id], (err) => {
                    if (err) {
                        console.log(err.message);
                    } else {
                        words[currentI].peaks = peaksString;
                        socket.emit('unlock');
                        console.log(`Peaks updated.`);
                    }
                });
            }
        });

        socket.on('setDivider', (data) => {
            const currentI = i - 1;
            if (currentI >= 0) {
                const sql = `UPDATE peaks SET divider=? WHERE word_id=?;`;
                db.run(sql, [parseInt(data.peakValue), words[currentI].id], (err) => {
                    if (err) {
                        console.log(err.message);
                    } else {
                        socket.emit('unlock');
                        console.log(`Divider inserted updated.`);
                    }
                });
            }
        });
    });
});

app.get('/synthesis', (req, res) => {
    res.sendFile(__dirname + '/synthesis.html');

    io.on('connection', (socket) => {
        socket.on('disconnect', () => {
            console.log('A user disconnected from synthesis.');
        });
    });
});

app.use(express.static('node_modules'));
app.use(express.static('css'));
app.use(express.static('words'));
app.use(express.static('lib'));

http.listen(3003, () => {
    console.log('listening on *:3003');
});
