#!/usr/bin/env zsh

cd mp3s

for name in *.mp3; do
    ffmpeg -i $name ../wavs/${name:r}.wav -y
done

cd ..

for name in wavs/*.wav; do
  echo $name; octave generate_peaks.m $name 
done

for name in wavs/*.wav; do
  cp $name /home/oh/FMFI/DIPLOMOVKA/synteza-slovenskeho-jazyka/words
done
