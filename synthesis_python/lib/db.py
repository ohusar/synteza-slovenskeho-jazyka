import sqlite3


class DB:
    PATH = '../db/synthesis.db'

    def __init__(self, db_path: str=PATH):
        self.connection = sqlite3.connect(db_path)
        self.cursor = None

    def execute(self, sql: str, variables: list=None):
        if variables is None:
            variables = []

        if self.cursor is None:
            self.cursor = self.connection.cursor()
        self.cursor.execute(sql, variables)


class DatabaseWrapper:
    db = DB()
