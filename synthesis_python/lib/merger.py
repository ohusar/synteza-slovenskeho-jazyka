from random import randint

import numpy as np

from scipy.io import wavfile

from synthesis_python.lib.constants import WINDOW_FN
from synthesis_python.lib.peak_tile import PeakTile
from synthesis_python.lib.sample import Sample
from synthesis_python.lib.word_type import WordType


DISTANCE = 386


class Merger:

    @staticmethod
    def merge_old_way(sample1: Sample, sample2: Sample):
        peak_tiles1 = sample1.peaks.peak_tiles(WordType.END)
        peak_tiles2 = sample2.peaks.peak_tiles(WordType.START)

        beginning = sample1.audio_data[:peak_tiles1[0].start]
        end = sample2.audio_data[peak_tiles2[-1].end + 1:]
        merged_peaks = Merger.merge_peaks(peak_tiles1, peak_tiles2, sample1.sample_rate)
        return np.concatenate((beginning, merged_peaks, end))

    @staticmethod
    def merge_to_word(samples: [Sample]):
        if len(samples) == 1:
            return samples[0].audio_data[samples[0].silence_start:samples[0].silence_end]
        if len(samples) == 2:
            return Merger.merge(samples[0], samples[1]).audio_data

        result = samples[0]
        for sample in samples[1:]:
            # Merger.write_result('{}-tmp'.format(randint(1, 213213)), result.audio_data, 48000)
            result = Merger.merge(result, sample)
        return result.audio_data

    @staticmethod
    def merge_to_sentence(words):
        silence = np.array([randint(0, 15) for _ in range(48000 // 1000)])
        result = words[0]
        for word in words[1:]:
            result = np.concatenate((result, silence))
            result = np.concatenate((result, word))
        return result

    @staticmethod
    def merge(sample1: Sample, sample2: Sample):
        peak_tiles1 = sample1.peaks.peak_tiles(WordType.END)
        peak_tiles2 = sample2.peaks.peak_tiles(WordType.START)

        beginning = sample1.audio_data[sample1.silence_start:peak_tiles1[0].start]
        end = sample2.audio_data[peak_tiles2[-1].end + 1:sample2.silence_end]

        Merger.write_result('beggining', beginning, 48000)
        Merger.write_result('end', end, 48000)

        merged_peak_tiles = Merger.merge_peaks(peak_tiles1, peak_tiles2, sample1.sample_rate)

        sample = Sample('-')
        sample.audio_data = np.concatenate((beginning, merged_peak_tiles, end))
        sample.sample_rate = sample1.sample_rate or sample2.sample_rate

        removed_end = len(sample2.audio_data) - sample2.silence_end
        extra_length = len(sample.audio_data) - len(sample2.audio_data) + removed_end
        sample2.peaks.update('-', extra_length, sample.audio_data)
        sample.peaks = sample2.peaks

        return sample

    @staticmethod
    def merge_peaks(peak_tiles1: [PeakTile], peak_tiles2: [PeakTile], sample_rate: int, speed_up=1):
        # distance_between_peaks = peak_tiles1[0].middle - peak_tiles1[0].start
        distance_between_peaks = DISTANCE
        needed_peaks_count = round(0.3 / (distance_between_peaks / sample_rate)) // 2

        def energy(arr):
            arr = [x**2 for x in arr]
            return sum(arr)

        def avg_energy(tiles):
            return sum([energy(t.audio_data) for t in tiles]) / len(tiles)

        equal_energy_tiles = peak_tiles1 + peak_tiles2
        for peak_tiles in [peak_tiles1, peak_tiles2]:
            for tile in peak_tiles:
                e = energy(tile.audio_data)
                coef = (avg_energy(equal_energy_tiles) / e)**(1/2)
                tile._full_audio_data = np.array(tile._full_audio_data) * coef
                tile.audio_data = tile._full_audio_data[tile.start:tile.end+1]

        peak_tiles_to_merge = Merger.choose_peak_tiles(peak_tiles1, peak_tiles2, needed_peaks_count)
        result, last_tile = peak_tiles_to_merge[0].audio_data, peak_tiles_to_merge[0]
        for tile in peak_tiles_to_merge[1:]:
            result, last_tile = Merger.append_peak_tile(result, last_tile, tile, distance_between_peaks)

        return result

    @staticmethod
    def append_peak_tile(result_so_far: np.array, last_tile: PeakTile, tile_to_append: PeakTile, distance: int):
        def energy(arr):
            arr = [x ** 2 for x in arr]
            return sum(arr)

        print(energy(tile_to_append.audio_data))

        middle_to_end = last_tile.end - last_tile.middle
        start_to_middle = tile_to_append.middle - tile_to_append.start

        overlap_size = middle_to_end + start_to_middle - distance

        window = WINDOW_FN(overlap_size * 2)
        decreasing = window[overlap_size:]
        increasing = window[:overlap_size+1]

        overlap1 = last_tile.full_audio_data[last_tile.end + 1 - overlap_size:last_tile.end + 1] * decreasing
        overlap2 = tile_to_append.full_audio_data[tile_to_append.start:tile_to_append.start + overlap_size] * increasing[:-1]
        overlap = overlap1 + overlap2

        result = np.concatenate((
            result_so_far[:-overlap_size],
            overlap,
            tile_to_append.full_audio_data[tile_to_append.start + overlap_size:tile_to_append.end + 1]
        ))

        return result, tile_to_append

    @staticmethod
    def choose_peak_tiles(peak_tiles1, peak_tiles2, needed_peaks_count):
        half = lambda x: x // 2
        half_with_modulo = lambda x: x // 2 + x % 2

        peak_tiles_to_merge = []
        peaks_from_first = half_with_modulo(needed_peaks_count) if \
            half_with_modulo(needed_peaks_count) <= len(peak_tiles1) else len(peak_tiles1)

        peaks_from_second = half(needed_peaks_count) if \
            half(needed_peaks_count) <= len(peak_tiles2) else len(peak_tiles2)

        for i in range(peaks_from_first):
            peak_tiles_to_merge.append(peak_tiles1[i])

        for i in range(peaks_from_second):
            peak_tiles_to_merge.append(peak_tiles2[i + len(peak_tiles2) - peaks_from_second])

        # PRESKAKOVANIE
        # first_current = half_with_modulo(peaks_from_first)
        # second_current = 0
        # first = True
        # while first_current < peaks_from_first:
        #     if first:
        #         peak_tiles_to_merge.append(peak_tiles1[first_current])
        #         first_current += 1
        #         first = False
        #     else:
        #         peak_tiles_to_merge.append(peak_tiles2[second_current])
        #         second_current += 1
        #         first = True

        return peak_tiles_to_merge  # + peak_tiles2[second_current:peaks_from_second]

    @staticmethod
    def write_result(name, result: np.array or list, sample_rate: int):
        wavfile.write('{}.wav'.format(name), sample_rate, np.array(result).astype(np.int16))
