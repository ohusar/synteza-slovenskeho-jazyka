from scipy.io import wavfile

from synthesis_python.lib.constants import ROOT_DIR
from synthesis_python.lib.db import DatabaseWrapper
from synthesis_python.lib.peaks import Peaks
from synthesis_python.lib.word_type import WordType


class Sample:
    SQL = """
      SELECT peaks, divider, file_path
      FROM words 
      JOIN peaks ON words.id = peaks.word_id
      JOIN samples ON words.id = samples.word_id
      WHERE word=?;
    """

    def __init__(self, word: str):
        self.word = word
        self.word_type = WordType.determine_type(word)
        self.peaks = None
        self.sample_rate, self.audio_data = None, None

        if word != ' ' and word != '-':
            self.load()

    def load(self):
        DatabaseWrapper.db.execute(Sample.SQL, [self.word])
        peaks, divider, file_path = DatabaseWrapper.db.cursor.fetchone()

        if '.mp3' in file_path:
            file_path = file_path.replace('mp3', 'wav')

        self.sample_rate, self.audio_data = wavfile.read('{}{}'.format(ROOT_DIR, file_path))
        self.load_peaks(peaks, divider)

    def load_peaks(self, peaks, divider=None):
        self.peaks = Peaks(self.word, peaks, self.audio_data, divider)

    @property
    def silence_start(self):
        start = 0
        for i in range(len(self.audio_data)):
            if abs(self.audio_data[i]) > 50:
                start = i
                break
        return start

    @property
    def silence_end(self):
        end = len(self.audio_data)
        for i in range(len(self.audio_data) - 1, -1, -1):
            if abs(self.audio_data[i]) > 2000:
                end = i
                break
        return end
