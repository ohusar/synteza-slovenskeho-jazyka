import numpy as np

from synthesis_python.lib.peak_tile import PeakTile
from synthesis_python.lib.word_type import WordType


class Peaks:
    def __init__(self, word: str, peaks: str, audio_data: np.array, divider: str or None):
        self.word = word
        self._raw = peaks
        self.audio_data = audio_data

        self.start_peak_tiles = None
        self.end_peak_tiles = None
        self.transform_to_peak_tiles(divider)

    @property
    def raw_peaks(self):
        return [int(peak) for peak in self._raw.split()]

    def __getitem__(self, item):
        return self.raw_peaks[item]

    def peak_tiles(self, word_type=None):
        if word_type == WordType.START:
            return self.start_peak_tiles or self.end_peak_tiles

        elif word_type == WordType.END:
            return self.end_peak_tiles or self.start_peak_tiles

        return self.start_peak_tiles or self.end_peak_tiles

    def transform_to_peak_tiles(self, divider):
        self.start_peak_tiles = []
        self.end_peak_tiles = []
        if divider:
            divider = self.raw_peaks.index(int(divider))
            for i in range(divider - 2):
                self.start_peak_tiles.append(PeakTile(self[i], self[i+1], self[i+2], self.audio_data))

            for i in range(divider, len(self.raw_peaks) - 2):
                self.end_peak_tiles.append(PeakTile(self[i], self[i+1], self[i+2], self.audio_data))
        else:
            for i in range(len(self.raw_peaks) - 2):
                self.start_peak_tiles.append(PeakTile(self[i], self[i + 1], self[i + 2], self.audio_data))
                self.end_peak_tiles.append(PeakTile(self[i], self[i + 1], self[i + 2], self.audio_data))

    def update(self, word, by, audio_data):
        self.word = word
        self.audio_data = audio_data
        self._raw = ' '.join([str(peak + by) for peak in self.raw_peaks])

        for peak_tile in self.end_peak_tiles:
            peak_tile.update(by, audio_data)


