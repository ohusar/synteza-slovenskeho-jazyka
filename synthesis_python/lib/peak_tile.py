
class PeakTile:
    def __init__(self, start: int, middle: int, end: int, audio_data):
        self.start, self.middle, self.end = start, middle, end
        self._full_audio_data = audio_data
        self.audio_data = self._full_audio_data[start:end+1]

    @property
    def full_audio_data(self):
        return self._full_audio_data

    def update(self, by, new_audio_data):
        self.start += by
        self.middle += by
        self.end += by

        self._full_audio_data = new_audio_data
        self.audio_data = new_audio_data[self.start:self.end+1]
