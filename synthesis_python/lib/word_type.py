from .constants import VOWELS
from unidecode import unidecode


class WordType:
    BOTH_ENDS = 0
    START = 1
    END = 2

    @staticmethod
    def determine_type(word):
        is_vowel = lambda char: unidecode(char.upper()) in VOWELS
        start = is_vowel(word[0])
        end = is_vowel(word[-1])

        if start and end:
            return WordType.BOTH_ENDS
        if start:
            return WordType.START
        if end:
            return WordType.END
        return None
