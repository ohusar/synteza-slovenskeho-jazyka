import numpy as np


ROOT_DIR = '../'
WINDOW_FNS = {
    'hamming': np.hamming,
    'hanning': np.hanning,
    'bartlett': np.bartlett,
    'blackman': np.blackman,
}
WINDOW_FN = WINDOW_FNS['bartlett']


VOWELS = {'A', 'E', 'I', 'O', 'U', 'Y'}
