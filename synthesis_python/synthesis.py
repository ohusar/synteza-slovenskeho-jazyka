from synthesis_python.lib.merger import Merger
from synthesis_python.lib.sample import Sample
from datetime import datetime

# SENTENCE = 'ahoj rozumies mi dobre v2'  # TODO get from input
# SAMPLE_NAMES = [('aho', 'oj'), ('rozu', 'umi', 'ies'), ('mi',), ('do', 'obre')]  # TODO parse algorithm
#
# samples = []
# words = []
# for word_samples in SAMPLE_NAMES:
#     samples = [Sample(sample) for sample in word_samples]
#     result = Merger.merge_to_word(samples)
#     words.append(result)
#
# merged = Merger.merge_to_sentence(words)
#
#
# def one_of_sample_rate(arr):
#     for el in arr:
#         if el.sample_rate:
#             return el.sample_rate
#     return 48000

sample1 = Sample('aho')
sample2 = Sample('oj')
merged1 = Merger.merge(sample1, sample2)
Merger.write_result('ahoj_{}'.format(datetime.now()), merged1.audio_data, sample1.sample_rate)

# Merger.write_result('_'.join(SENTENCE.split()), merged, one_of_sample_rate(samples))
