addpath ./mex-sqlite3-master;
savepath;

SQL = "SELECT words.id, word, file_path, peaks FROM words \
JOIN samples ON words.id = samples.word_id \
JOIN peaks ON words.id = peaks.word_id;";

words = sqlite3(
  "../db/synthesis.db",
  SQL
);
