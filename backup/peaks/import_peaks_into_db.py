from os import listdir
from os.path import isfile, join
import sqlite3

main_path = '/home/oh/FMFI/DIPLOMOVKA/synteza-slovenskeho-jazyka/'
peaks_path = '{}peaks/peaks/'.format(main_path)
directory_files = [f for f in listdir(peaks_path) if isfile(join(peaks_path, f))]

def parse_file(file_name):
    with open('{}{}'.format(peaks_path, file_name)) as file:
        result = []
        for line in file:
            _, index = line.split()
            result.append(round(float(index)))
        return ' '.join([str(item) for item in result])

conn = sqlite3.connect('{}db/synthesis.db'.format(main_path))

for file_name in directory_files:
    result = parse_file(file_name)

    word = file_name.split('.')[0]
    word_select = "SELECT id FROM words WHERE word='{}';".format(word)
    for select_result in conn.execute(word_select):
        word_id = select_result[0]
    try:
        exc_result = conn.execute("INSERT INTO peaks(word_id, peaks) VALUES (?, ?)", [word_id, result])
        print('Insert', result[:3], '... for word', word, 'and word_id:', word_id, '.')
    except sqlite3.IntegrityError:
        print('Already exists for', word)

conn.commit()
conn.close()
