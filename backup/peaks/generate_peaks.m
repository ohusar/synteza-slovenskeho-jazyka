warning('off','all');

addpath /home/oh/FMFI/DIPLOMOVKA/tempo/signal/inst

arg_list = argv ();

function [res] = zero_cross_rate(in)
  res = [];
  n = length(in);
  ms = 25;
  zmena = floor(ms*10);
  posun = 0;
  zaciatok = 1;
  koniec = zaciatok + zmena;
  index = zaciatok;
  test = 1;
  while test == 1
    if(koniec > n)
      koniec = n;
      test = 0;
    end;
    sucasne = [];
    tmp = in(zaciatok:koniec);
    counter = 0;
    for ii = 2:length(tmp)
      if(((tmp(ii) < 0) && (tmp(ii-1) > 0)) || ((tmp(ii) > 0) && (tmp(ii-1) < 0)))
        counter = counter + 1;
      end
    end
    pom = counter/length(tmp);
    res = [res; pom index];
    zaciatok = zaciatok + zmena;
    koniec = koniec + zmena;
    index = index + zmena;
  end
end

function [peaky] = najdi_peaky(vect, zcr)
  ms = 25;
  zmena = floor(ms*10);
  n = length(vect);
  test = 1;
  zaciatok = 1;
  peaky = [];
  korelacie = [];
  posun = 0;
  res = 0;
  koniec = zaciatok + zmena;
  iteracia = 0;
  frekvencie = [];
  prejdenaVzdialenost = 0;
  while test == 1
    if(koniec > n)
      koniec = n;
      test = 0;
    end;
    sucasne = [];
    tmp = vect(zaciatok:koniec);
    res = xcorr(tmp);
    klesa = 1;
    iter = floor(length(res)/2) - 2;
    while(klesa == 1)
      if (iter > 2)
        if (res(iter - 1) > res(iter))
          klesa = 0;
        end
      iter = iter - 1;
      end
      if (iteracia > length(tmp))
        klesa = 0;
      end
      iteracia++;
    end
    [a, b] = max(res(1:iter));
    if(a < 0.025)
      zaciatok = zaciatok + zmena;
      koniec = koniec + zmena;
      posun = posun + zmena;
      continue
    end
    for ii = b:b:length(tmp)
      zac = ii - b + 1;
      kon = ii;
      [c, d] = max(tmp(zac:kon));
      norm = d+zac-1;
      sucasne = [sucasne; c posun+norm];
    end
    [c, d] = max(tmp(ii:end));
    sucasne = [sucasne; c posun+d+ii-1];
    avg = mean(sucasne);
    for ii = 1:length(sucasne)
      c = sucasne(ii, 1);
      d = sucasne(ii, 2);
      if(c > (avg(1)*0.95))
        if (length(frekvencie) == 0)
          frekvencie = [d];
          peaky = [peaky; c d];
        else
          frekvencia = d - peaky(length(frekvencie),2);
          if(frekvencia > 250)
            frekvencie = [frekvencie; d - peaky(length(frekvencie),2)];
            peaky = [peaky; c d];
          end
        end
      end
    end
    zaciatok = zaciatok + zmena;
    koniec = koniec + zmena;
    posun = posun + zmena;
  end
  frekvencie;
end

function [peaky] = run(filename)
	[vect Fs Bps] = wavread(filename);

	rozmery = size(vect);
	if (rozmery(1) > rozmery(2))
	  vect = vect';
	end

	vect = vect';

	zcr = zero_cross_rate(vect);
	peaky = najdi_peaky(vect, zcr);
end

R = run(arg_list{1});
file_name = sprintf('peaks/%s.peaks', strrep(arg_list{1}, "wavs/", ""));
save(file_name, '-ascii', 'R');

#R = run("wavs/otvo.wav");
#save("test.test", 'R');