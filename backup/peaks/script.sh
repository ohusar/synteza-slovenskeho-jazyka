#!/usr/bin/env zsh

cd mp3s

for name in *.mp3; do
    ffmpeg -i $name ../wavs/${name:r}.wav &>/dev/null
done

cd ..

for name in wavs/*.wav; do
  echo $name; octave generate_peaks.m $name &>/dev/null
done

for name in wavs/*.wav; do
  cp $name /home/oh/FMFI/DIPLOMOVKA/synteza-slovenskeho-jazyka/words
done
