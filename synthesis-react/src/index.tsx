
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import './index.css';

import Synthesis from './synthesis/synthesis';
import DatabaseProvider from './database/database';

ReactDOM.render(
    <DatabaseProvider>
        <Synthesis />
    </DatabaseProvider>,
    document.getElementById('root') as HTMLElement
);

registerServiceWorker();
