import * as React from 'react';
import styles from './synthesis.module.css';

import {DatabaseConsumer} from '../database/database';

class Synthesis extends React.Component {
    render() {
        return (
            <DatabaseConsumer>
                {db => {
                    if (db) {
                        let sql = `SELECT id, word FROM words;`;
                        let words: string[] = [];
                        db.all(sql, [], (err, rows) => {
                            if (rows) {
                                words = rows;
                            }
                        });
                        return JSON.stringify(words);
                    }
                    return null;

                }}
            </DatabaseConsumer>
        );
    };
}

export default Synthesis;
