/* tslint:disable */
// This file was automatically generated and should not be edited.
export interface SynthesisStyles {
	'test': string;
}

export type ISynthesisStyles = SynthesisStyles;
export const locals: SynthesisStyles;
export default locals;
