import * as React from 'react';
import * as sqlite3 from 'sqlite3';
import {Database} from 'sqlite3';
import * as PropTypes from 'prop-types';

const DatabaseContext = React.createContext<Database | undefined>(undefined);
export const DatabaseConsumer = DatabaseContext.Consumer;

interface State {
    db?: Database;
}

class DatabaseProvider extends React.Component<{}, State> {
    componentDidMount() {
        const db = new sqlite3.Database('/home/oh/FMFI/DIPLOMOVKA/synteza-slovenskeho-jazyka/db/synthesis.db', (err) => {
            if (err) {
                console.error(err.message);
                return;
            }
            console.log('Connected to the SQlite database.');
        });
        this.setState({db});
    }

    render() {
        return (
            <DatabaseContext.Provider value={this.state.db}>
                {this.props.children}
            </DatabaseContext.Provider>
        );
    }
}

export default DatabaseProvider;
